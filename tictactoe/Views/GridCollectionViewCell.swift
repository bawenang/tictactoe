//
//  GridCollectionViewCell.swift
//  tictactoe
//
//  Created by Bawenang RPP on 18/06/19.
//  Copyright © 2019 Bawenang. All rights reserved.
//

import UIKit

class GridCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellTypeLabel: UILabel!
    
    func setup(renderCell: RenderCell) {
        setLabel(with: renderCell)
    }
    
    private func setLabel(with renderCell: RenderCell) {
        switch renderCell {
        case .empty:
            cellTypeLabel.text = ""
        case .x:
            cellTypeLabel.text = "X"
            cellTypeLabel.textColor = UIColor.blue
        case .o:
            cellTypeLabel.text = "O"
            cellTypeLabel.textColor = UIColor.red
        }
    }
    
}
