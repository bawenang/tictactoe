//
//  BoardViewModelTest.swift
//  tictactoeTests
//
//  Created by Bawenang RPP on 18/06/19.
//  Copyright © 2019 Bawenang. All rights reserved.
//

import XCTest
import RxSwift
import RxCocoa

@testable import tictactoe

class BoardViewModelTest: XCTestCase {
    private let boardLogicMock = BoardLogicMock()
    private var boardViewModel: BoardViewModel!
    
    private var disposeBag = DisposeBag()
    
    override func setUp() {
        boardViewModel = BoardViewModel(boardLogic: boardLogicMock)
        disposeBag = DisposeBag()
    }

    func testViewDidLoad() {
        boardViewModel.eventActivePlayer
            .asObservable()
            .subscribe(onNext: { activePlayer in
                XCTAssertEqual(activePlayer, .playerOne)
            }).disposed(by: disposeBag)
        
        let expectSetBoard = expectation(description: "should toggle set board")
        boardViewModel.eventSetBoard
            .asObservable()
            .subscribe(onNext: {
                expectSetBoard.fulfill()
            }).disposed(by: disposeBag)
        
        let emptyGrid = Array<RenderCell>(repeating: .empty, count: 9)
        boardLogicMock.setRenderCells(emptyGrid)
        
        boardViewModel.viewDidLoad()
        wait(for: [expectSetBoard], timeout: 0.1)
        assertGridMustBeEmpty(boardViewModel: boardViewModel)
    }
    
    func testDidSelectCell() {
        let expectActivePlayer = expectation(description: "should toggle active player")
        expectActivePlayer.expectedFulfillmentCount = 3
        var eventActivePlayerCount = 0
        boardViewModel.eventActivePlayer
            .asObservable()
            .subscribe(onNext: { activePlayer in
                switch eventActivePlayerCount {
                case 0:
                    XCTAssertEqual(activePlayer, .playerOne)
                case 1:
                    XCTAssertEqual(activePlayer, .playerTwo)
                case 2:
                    XCTAssertEqual(activePlayer, .playerOne)
                default:
                    XCTFail("Should not toggle active player more than 3 times!")
                }
                
                eventActivePlayerCount += 1
                expectActivePlayer.fulfill()
            }).disposed(by: disposeBag)
        
        let expectSetBoard = expectation(description: "should do set board")
        expectSetBoard.expectedFulfillmentCount = 3
        boardViewModel.eventSetBoard
            .asObservable()
            .subscribe(onNext: {
                expectSetBoard.fulfill()
            }).disposed(by: disposeBag)
        
        var finalGrid = Array<RenderCell>(repeating: .empty, count: 9)
        finalGrid[0] = .x
        finalGrid[1] = .o
        boardLogicMock.setRenderCells(finalGrid)
        
        boardViewModel.viewDidLoad()
        boardViewModel.didSelectCell(at: 0)
        boardViewModel.didSelectCell(at: 1)
        
        wait(for: [expectActivePlayer, expectSetBoard], timeout: 0.1)
        XCTAssertEqual(boardLogicMock.chooseCellCallCounter, 2)
    }
    
    func testDidSelectCellOnInvalidCell() {
        let expectActivePlayer = expectation(description: "should toggle active player")
        expectActivePlayer.expectedFulfillmentCount = 2
        var eventActivePlayerCount = 0
        boardViewModel.eventActivePlayer
            .asObservable()
            .subscribe(onNext: { activePlayer in
                switch eventActivePlayerCount {
                case 0:
                    XCTAssertEqual(activePlayer, .playerOne)
                case 1:
                    XCTAssertEqual(activePlayer, .playerTwo)
                default:
                    XCTFail("Should not toggle active player more than 2 times!")
                }
                
                eventActivePlayerCount += 1
                expectActivePlayer.fulfill()
            }).disposed(by: disposeBag)
        
        let expectSetBoard = expectation(description: "should do set board")
        expectSetBoard.expectedFulfillmentCount = 2
        boardViewModel.eventSetBoard
            .asObservable()
            .subscribe(onNext: {
                expectSetBoard.fulfill()
            }).disposed(by: disposeBag)
        
        boardViewModel.viewDidLoad()
        
        var finalGrid = Array<RenderCell>(repeating: .empty, count: 9)
        finalGrid[0] = .x
        boardLogicMock.setRenderCells(finalGrid)
        boardViewModel.didSelectCell(at: 0)
        
        boardLogicMock.setShouldThrowError(true)
        boardViewModel.didSelectCell(at: 0)
        
        wait(for: [expectActivePlayer, expectSetBoard], timeout: 0.1)
        XCTAssertEqual(boardLogicMock.chooseCellCallCounter, 1)
    }
    
    func testCellForItem() {
        let renderGrid: [RenderCell] = [
            .x, .o, .empty,
            .empty, .x, .o,
            .o, .x, .empty
        ]
        boardLogicMock.setRenderCells(renderGrid)
        
        XCTAssertEqual( boardViewModel.cellForItem(at: 0), .x)
        XCTAssertEqual( boardViewModel.cellForItem(at: 1), .o)
        XCTAssertEqual( boardViewModel.cellForItem(at: 2), .empty)
    }
    
    func testDidSelectCellWhenWinningStatePlayerOne() {
        boardLogicMock.setWinningStateOfNextDidSelectResult(.playerOne)
        
        let expectPlayerOneWins = expectation(description: "player 1 should win")
        boardViewModel.eventPlayerOneWins
            .asObservable()
            .subscribe(onNext: {
                expectPlayerOneWins.fulfill()
            }).disposed(by: disposeBag)
        
        let expectPlayerTwoNotWins = expectation(description: "player 2 should not win")
        expectPlayerTwoNotWins.isInverted = true
        boardViewModel.eventPlayerTwoWins
            .asObservable()
            .subscribe(onNext: {
                expectPlayerTwoNotWins.fulfill()
            }).disposed(by: disposeBag)
        
        let expectNotDraw = expectation(description: "should not draw")
        expectNotDraw.isInverted = true
        boardViewModel.eventDraw
            .asObservable()
            .subscribe(onNext: {
                expectNotDraw.fulfill()
            }).disposed(by: disposeBag)
        
        let expectActivePlayerNotToggled = expectation(description: "shouldn't toggle active player")
        expectActivePlayerNotToggled.isInverted = true
        boardViewModel.eventActivePlayer
            .asObservable()
            .subscribe(onNext: { _ in
                expectActivePlayerNotToggled.fulfill()
            }).disposed(by: disposeBag)
        
        boardViewModel.didSelectCell(at: 0)
        
        wait(for: [expectPlayerOneWins,
                   expectPlayerTwoNotWins,
                   expectNotDraw,
                   expectActivePlayerNotToggled],
             timeout: 1)
        
        XCTAssertEqual(boardLogicMock.chooseCellCallCounter, 1)
    }
    
    func testDidSelectCellWhenWinningStatePlayerTwo() {
        boardLogicMock.setWinningStateOfNextDidSelectResult(.playerTwo)
        
        let expectPlayerOneNotWins = expectation(description: "player 1 should not win")
        expectPlayerOneNotWins.isInverted = true
        boardViewModel.eventPlayerOneWins
            .asObservable()
            .subscribe(onNext: {
                expectPlayerOneNotWins.fulfill()
            }).disposed(by: disposeBag)
        
        let expectPlayerTwoWins = expectation(description: "player 2 should win")
        boardViewModel.eventPlayerTwoWins
            .asObservable()
            .subscribe(onNext: {
                expectPlayerTwoWins.fulfill()
            }).disposed(by: disposeBag)
        
        let expectNotDraw = expectation(description: "should not draw")
        expectNotDraw.isInverted = true
        boardViewModel.eventDraw
            .asObservable()
            .subscribe(onNext: {
                expectNotDraw.fulfill()
            }).disposed(by: disposeBag)
        
        let expectActivePlayerNotToggled = expectation(description: "should not toggle activeplayer.")
        expectActivePlayerNotToggled.isInverted = true
        boardViewModel.eventActivePlayer
            .asObservable()
            .subscribe(onNext: { _ in
                expectActivePlayerNotToggled.fulfill()
            }).disposed(by: disposeBag)
        
        boardViewModel.didSelectCell(at: 0)
        
        wait(for: [expectPlayerOneNotWins,
                   expectPlayerTwoWins,
                   expectNotDraw,
                   expectActivePlayerNotToggled],
             timeout: 1)
        
        XCTAssertEqual(boardLogicMock.chooseCellCallCounter, 1)
    }
    
    func testDisableDidSelectCellWhenPlayerAlreadyWin() {
        boardLogicMock.setWinningStateOfNextDidSelectResult(.playerOne)
        
        let expectPlayerOneWins = expectation(description: "player 1 should win only once")
        boardViewModel.eventPlayerOneWins
            .asObservable()
            .subscribe(onNext: {
                expectPlayerOneWins.fulfill()
            }).disposed(by: disposeBag)
        
        let expectPlayerTwoNotWins = expectation(description: "player 2 should not win")
        expectPlayerTwoNotWins.isInverted = true
        boardViewModel.eventPlayerTwoWins
            .asObservable()
            .subscribe(onNext: {
                expectPlayerTwoNotWins.fulfill()
            }).disposed(by: disposeBag)
        
        let expectNotDraw = expectation(description: "should not draw")
        expectNotDraw.isInverted = true
        boardViewModel.eventDraw
            .asObservable()
            .subscribe(onNext: {
                expectNotDraw.fulfill()
            }).disposed(by: disposeBag)
        
        let expectSetBoard = expectation(description: "should do set board once when didSelectCell(at:) the first time.")
        boardViewModel.eventSetBoard
            .asObservable()
            .subscribe(onNext: {
                expectSetBoard.fulfill()
            }).disposed(by: disposeBag)
        
        let expectActivePlayerNotToggled = expectation(description: "should not toggle activeplayer.")
        expectActivePlayerNotToggled.isInverted = true
        boardViewModel.eventActivePlayer
            .asObservable()
            .subscribe(onNext: { _ in
                expectActivePlayerNotToggled.fulfill()
            }).disposed(by: disposeBag)
        
        boardViewModel.didSelectCell(at: 0)
        boardViewModel.didSelectCell(at: 1)
        
        wait(for: [expectPlayerOneWins,
                   expectPlayerTwoNotWins,
                   expectNotDraw,
                   expectActivePlayerNotToggled,
                   expectSetBoard],
             timeout: 1)
        
        XCTAssertEqual(boardLogicMock.chooseCellCallCounter, 1)
    }
    
    func testDidSelectCellWhenDraw() {
        boardLogicMock.setWinningStateOfNextDidSelectResult(.draw)
        
        let expectPlayerOneNotWins = expectation(description: "player 1 should not win")
        expectPlayerOneNotWins.isInverted = true
        boardViewModel.eventPlayerOneWins
            .asObservable()
            .subscribe(onNext: {
                expectPlayerOneNotWins.fulfill()
            }).disposed(by: disposeBag)
        
        let expectPlayerTwoNotWins = expectation(description: "player 2 should not win")
        expectPlayerTwoNotWins.isInverted = true
        boardViewModel.eventPlayerTwoWins
            .asObservable()
            .subscribe(onNext: {
                expectPlayerTwoNotWins.fulfill()
            }).disposed(by: disposeBag)
        
        let expectDraw = expectation(description: "should draw")
        boardViewModel.eventDraw
            .asObservable()
            .subscribe(onNext: {
                expectDraw.fulfill()
            }).disposed(by: disposeBag)
        
        let expectActivePlayerNotToggled = expectation(description: "should not toggle activeplayer.")
        expectActivePlayerNotToggled.isInverted = true
        boardViewModel.eventActivePlayer
            .asObservable()
            .subscribe(onNext: { _ in
                expectActivePlayerNotToggled.fulfill()
            }).disposed(by: disposeBag)
        
        boardViewModel.didSelectCell(at: 0)
        
        wait(for: [expectPlayerOneNotWins,
                   expectPlayerTwoNotWins,
                   expectDraw,
                   expectActivePlayerNotToggled],
             timeout: 1)
        
        XCTAssertEqual(boardLogicMock.chooseCellCallCounter, 1)
    }
    
    func testDisableDidSelectCellWhenAlreadyDraw() {
        boardLogicMock.setWinningStateOfNextDidSelectResult(.draw)
        
        let expectPlayerOneNotWins = expectation(description: "player 1 should not win")
        expectPlayerOneNotWins.isInverted = true
        boardViewModel.eventPlayerOneWins
            .asObservable()
            .subscribe(onNext: {
                expectPlayerOneNotWins.fulfill()
            }).disposed(by: disposeBag)
        
        let expectPlayerTwoNotWins = expectation(description: "player 2 should not win")
        expectPlayerTwoNotWins.isInverted = true
        boardViewModel.eventPlayerTwoWins
            .asObservable()
            .subscribe(onNext: {
                expectPlayerTwoNotWins.fulfill()
            }).disposed(by: disposeBag)
        
        let expectDraw = expectation(description: "should draw")
        boardViewModel.eventDraw
            .asObservable()
            .subscribe(onNext: {
                expectDraw.fulfill()
            }).disposed(by: disposeBag)
        
        let expectSetBoard = expectation(description: "should do set board once when didSelectCell(at:) the first time.")
        boardViewModel.eventSetBoard
            .asObservable()
            .subscribe(onNext: {
                expectSetBoard.fulfill()
            }).disposed(by: disposeBag)
        
        let expectActivePlayerNotToggled = expectation(description: "should not toggle activeplayer.")
        expectActivePlayerNotToggled.isInverted = true
        boardViewModel.eventActivePlayer
            .asObservable()
            .subscribe(onNext: { _ in
                expectActivePlayerNotToggled.fulfill()
            }).disposed(by: disposeBag)
        
        boardViewModel.didSelectCell(at: 0)
        boardViewModel.didSelectCell(at: 1)
        
        wait(for: [expectPlayerOneNotWins,
                   expectPlayerTwoNotWins,
                   expectDraw,
                   expectActivePlayerNotToggled,
                   expectSetBoard],
             timeout: 1)
        
        XCTAssertEqual(boardLogicMock.chooseCellCallCounter, 1)
    }
    
    // MARK:-
    
    private func assertGridMustBeEmpty(boardViewModel: BoardViewModel) {
        for index in 0...8 {
            XCTAssertEqual(boardViewModel.cellForItem(at: index), .empty)
        }
    }

    private class BoardLogicMock: BoardLogic {

        private var renderCells = [RenderCell]()
        private var isError = false
        private var winningState = WinningState.none
        private(set) var chooseCellCallCounter = 0
        
        func chooseCell(index: Int, for player: ActivePlayer) throws{
            
            if isError {
                throw LogicError.chooseNotEmptyCell
            }
            
            chooseCellCallCounter += 1
        }
        
        func getRenderCell(at index: Int) -> RenderCell {
            return renderCells[index]
        }
    
        func getWinningState() -> WinningState {
            return winningState
        }
        
        // MARK:-
        
        func setRenderCells(_ renderCells: [RenderCell]) {
            self.renderCells = renderCells
            isError = false
        }
        
        func setShouldThrowError(_ isError: Bool) {
            self.isError = isError
        }
        
        func setWinningStateOfNextDidSelectResult(_ winningState: WinningState) {
            self.winningState = winningState
        }
    }
}
