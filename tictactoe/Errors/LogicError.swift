//
//  LogicError.swift
//  tictactoe
//
//  Created by Bawenang RPP on 21/06/19.
//  Copyright © 2019 Bawenang. All rights reserved.
//

enum LogicError: Error {
    case chooseNotEmptyCell
}
