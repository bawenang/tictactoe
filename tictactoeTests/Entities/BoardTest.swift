//
//  BoardTest.swift
//  tictactoeTests
//
//  Created by Bawenang RPP on 21/06/19.
//  Copyright © 2019 Bawenang. All rights reserved.
//

import XCTest

@testable import tictactoe

class BoardTest: XCTestCase {
    
    func testInit() {
        let grid = Array<Cell>(repeating: .empty, count: 9)
        guard let _ = BoardImpl(grid: grid) else {
            XCTFail("should check for 9 cells grid")
            return
        }
    }
    
    func testInitWithInvalidGrid() {
        let gridWithLessThan9Items = Array<Cell>(repeating: .empty, count: 8)
        XCTAssertNil(BoardImpl(grid: gridWithLessThan9Items))
        
        let gridWithMoreThan9Items = Array<Cell>(repeating: .empty, count: 10)
        XCTAssertNil(BoardImpl(grid: gridWithMoreThan9Items))
    }
    
    func testGetCell() {
        var grid = Array<Cell>(repeating: .empty, count: 9)
        grid[0] = .playerOne
        grid[1] = .playerTwo
        guard let board = BoardImpl(grid: grid) else {
            XCTFail("should check for 9 cells grid")
            return
        }
        
        XCTAssertEqual(board.getCell(at: 0), .playerOne)
        XCTAssertEqual(board.getCell(at: 1), .playerTwo)
        XCTAssertEqual(board.getCell(at: 2), .empty)
    }
    
    func testGetGrid() {
        let grid: [Cell] = [
            .playerOne, .playerTwo, .empty,
            .empty, .playerOne, .playerTwo,
            .playerTwo, .empty, .playerOne
        ]
        guard let board = BoardImpl(grid: grid) else {
            XCTFail("should check for 9 cells grid")
            return
        }
        
        XCTAssertEqual(board.getCell(at: 0), .playerOne)
        XCTAssertEqual(board.getCell(at: 1), .playerTwo)
        XCTAssertEqual(board.getCell(at: 2), .empty)
    }
    
    func testSetCell() {
        let grid = Array<Cell>(repeating: .empty, count: 9)
        guard let board = BoardImpl(grid: grid) else {
            XCTFail("should check for 9 cells grid")
            return
        }
        
        do {
            try board.setCell(at: 0, cellType: .playerOne)
            try board.setCell(at: 1, cellType: .playerTwo)
            
            XCTAssertEqual(board.getCell(at: 0), .playerOne)
            XCTAssertEqual(board.getCell(at: 1), .playerTwo)
        } catch {
            XCTFail("should not happen")
        }
    }
    
    func testSetCellForNonEmptyCellShouldThrowError() {
        let grid = Array<Cell>(repeating: .empty, count: 9)
        guard let board = BoardImpl(grid: grid) else {
            XCTFail("should check for 9 cells grid")
            return
        }
        
        do {
            try board.setCell(at: 0, cellType: .playerOne)
            XCTAssertEqual(board.getCell(at: 0), .playerOne)
            
            try board.setCell(at: 0, cellType: .playerTwo)
            
            XCTFail("should not happen and throw error instead")
        } catch let error {
            guard let error = error as? LogicError else {
                XCTFail("Wrong typ of error caught")
                return
            }
            XCTAssertEqual(error, LogicError.chooseNotEmptyCell)
        }
    }
    
    func testCreateDefault() {
        let board = BoardImpl.createDefault()
        
        for index in 0...8 {
            XCTAssertEqual(board.getCell(at: index), .empty)
        }
    }

}
