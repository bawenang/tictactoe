//
//  MainmenuViewController.swift
//  tictactoe
//
//  Created by Maven on 25/01/19.
//  Copyright © 2019 Bawenang. All rights reserved.
//

import UIKit
import RxSwift

class MainmenuViewController: UIViewController {
    
    private let segueMainToMode = "segueMainToMode"
    
    @IBAction func onStartButton(_ sender: Any) {
        performSegue(withIdentifier: segueMainToMode, sender: nil)
    }

}
