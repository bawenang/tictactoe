//
//  BoardLogic.swift
//  tictactoe
//
//  Created by Bawenang RPP on 21/06/19.
//  Copyright © 2019 Bawenang. All rights reserved.
//

enum ActivePlayer {
    case playerOne
    case playerTwo
}

enum RenderCell {
    case empty
    case x
    case o
}

enum WinningState {
    case none
    case playerOne
    case playerTwo
    case draw
}

protocol BoardLogic {
    func chooseCell(index: Int, for player: ActivePlayer) throws
    func getRenderCell(at index: Int) -> RenderCell
    func getWinningState() -> WinningState
}

class BoardLogicImpl: BoardLogic {
    
    private var board: Board
    
    init(board: Board) {
        self.board = board
    }
    
    func chooseCell(index: Int, for player: ActivePlayer) throws {
        do {
            try board.setCell(at: index, cellType: Cell.create(from: player))
        } catch let error {
            throw error
        }
    }
    
    func getRenderCell(at index: Int) -> RenderCell {
        return RenderCell.create(from: board.getCell(at: index))
    }
    
    func getWinningState() -> WinningState {
        var winningState = WinningState.none
        
        winningState = checkHorizontalWin()
        
        if winningState == .none {
            winningState = checkVerticalWin()
        }
        
        if winningState == .none {
            winningState = checkDiagonalWin()
        }
        
        if winningState == .none {
            winningState = checkDraw()
        }
        
        return winningState
    }
    
    private func mapRenderCells(from board: Board) -> [RenderCell] {
        return board.getGrid().map { RenderCell.create(from: $0) }
    }

    private func checkHorizontalWin() -> WinningState {
        let grid = board.getGrid()

        if grid[0] != .empty && grid[0] == grid[1] && grid[0] == grid[2] {
            return mapToWinningState(cell: grid[0])
        } else if grid[3] != .empty && grid[3] == grid[4] && grid[3] == grid[5] {
            return mapToWinningState(cell: grid[3])
        } else if grid[6] != .empty && grid[6] == grid[7] && grid[6] == grid[8] {
            return mapToWinningState(cell: grid[6])
        }

        return .none
    }

    private func checkVerticalWin() -> WinningState {
        let grid = board.getGrid()

        if grid[0] != .empty && grid[0] == grid[3] && grid[0] == grid[6] {
            return mapToWinningState(cell: grid[0])
        } else if grid[1] != .empty && grid[1] == grid[4] && grid[1] == grid[7] {
            return mapToWinningState(cell: grid[1])
        } else if grid[2] != .empty && grid[2] == grid[5] && grid[2] == grid[8] {
            return mapToWinningState(cell: grid[2])
        }

        return .none
    }

    private func checkDiagonalWin() -> WinningState {
        let grid = board.getGrid()

        if grid[0] != .empty && grid[0] == grid[4] && grid[0] == grid[8] {
            return mapToWinningState(cell: grid[0])
        } else if grid[2] != .empty && grid[2] == grid[4] && grid[2] == grid[6] {
            return mapToWinningState(cell: grid[2])
        }

        return .none
    }
    
    private func checkDraw() -> WinningState {
        let grid = board.getGrid()
        
        if !grid.contains(.empty) {
            return .draw
        }

        return .none
    }
    
    private func mapToWinningState(cell: Cell) -> WinningState {
        switch cell {
        case .empty:
            return .none
        case .playerOne:
            return .playerOne
        case .playerTwo:
            return .playerTwo
            
        }
    }
}

extension RenderCell {
    static func create(from cell: Cell) -> RenderCell {
        switch cell {
        case .empty:
            return .empty
        case .playerOne:
            return .x
        case.playerTwo:
            return .o
        }
    }
}
