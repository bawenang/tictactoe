//
//  ModemenuViewController.swift
//  tictactoe
//
//  Created by Bawenang RPP on 18/06/19.
//  Copyright © 2019 Bawenang. All rights reserved.
//

import UIKit

class ModemenuViewController: UIViewController {
    
    private let segueModeToBoard = "segueModeToBoard"
    
    @IBAction func onSinglePlayerButton(_ sender: Any) {
    }
    
    @IBAction func onTwoPlayersButton(_ sender: Any) {
        performSegue(withIdentifier: segueModeToBoard, sender: nil)
    }
    
    @IBAction func onCancelButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
