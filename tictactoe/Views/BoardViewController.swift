//
//  BoardViewController.swift
//  tictactoe
//
//  Created by Bawenang RPP on 18/06/19.
//  Copyright © 2019 Bawenang. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BoardViewController: UIViewController {
    private let reuseNibName = "GridCollectionViewCell"
    private let reuseIdentifier = "gridCollectionViewCell"

    @IBOutlet weak var player_1_view: UIView!
    @IBOutlet weak var player_2_view: UIView!
    @IBOutlet weak var gridCollectionView: UICollectionView!
    
    private var boardViewModel: BoardViewModel!
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let grid = Array<Cell>(repeating: .empty, count: 9)
        let boardLogic = BoardLogicImpl(board: BoardImpl(grid: grid)!)
        boardViewModel = BoardViewModel(boardLogic: boardLogic)
        gridCollectionView.register(UINib.init(nibName: reuseNibName, bundle: nil),
                                    forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
        //gridCollectionView.reloadData()
        gridCollectionView.delegate = self
        gridCollectionView.dataSource = self
        
        setupEvents()
        
        boardViewModel.viewDidLoad()
    }
    
    private func setupEvents() {
        boardViewModel.eventActivePlayer
            .asDriver(onErrorJustReturn: nil)
            .drive(onNext: { [weak self] activePlayer in
                guard let activePlayer = activePlayer else { return }
                switch activePlayer {
                case .playerOne:
                    self?.player_1_view.backgroundColor = UIColor(red: 0.85, green: 0.9, blue: 1, alpha: 1)
                    self?.player_2_view.backgroundColor = UIColor.white
                case .playerTwo:
                    self?.player_1_view.backgroundColor = UIColor.white
                    self?.player_2_view.backgroundColor = UIColor(red: 0.85, green: 0.9, blue: 1, alpha: 1)
                }
            }).disposed(by: disposeBag)
        
        boardViewModel.eventSetBoard
            .observeOn(MainScheduler.instance)
            .asObservable()
            .subscribe(onNext: { [weak self] in
                self?.gridCollectionView.reloadData()
            }).disposed(by: disposeBag)
        
        boardViewModel.eventPlayerOneWins
            .observeOn(MainScheduler.instance)
            .asObservable()
            .subscribe(onNext: { [weak self] in
                guard let `self` = self else { return }
                let alert = self.createWinningAlert(title: "Congratulations", message: "Player 1 wins!")
                self.present(alert, animated: true, completion: nil)            })
            .disposed(by: disposeBag)
        
        boardViewModel.eventPlayerTwoWins
            .observeOn(MainScheduler.instance)
            .asObservable()
            .subscribe(onNext: { [weak self] in
                guard let `self` = self else { return }
                let alert = self.createWinningAlert(title: "Congratulations", message: "Player 2 wins!")
                self.present(alert, animated: true, completion: nil)
            })
            .disposed(by: disposeBag)
        
        boardViewModel.eventDraw
            .observeOn(MainScheduler.instance)
            .asObservable()
            .subscribe(onNext: { [weak self] in
                guard let `self` = self else { return }
                let alert = self.createWinningAlert(title: "Too bad", message: "It's a draw!")
                self.present(alert, animated: true, completion: nil)
            })
            .disposed(by: disposeBag)
    }
    
    private func createWinningAlert(title: String, message: String) -> UIAlertController {
        let alert = UIAlertController(title: title,
                                      message: message, preferredStyle: .alert)
        
        let okButton = UIAlertAction(title: "Ok",
                                     style: .cancel, handler: { [weak self] _ in
                                        self?.dismiss(animated: true, completion: nil)
        })
        
        alert.addAction(okButton)
        
        return alert
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BoardViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        boardViewModel.didSelectCell(at: indexPath.oneDimensionalIndexFromSectionRow())
    }
}


extension BoardViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier,
                                                      for: indexPath)
        
        guard let gridCell = cell as? GridCollectionViewCell else { return cell }
        
        // Configure the cell
        gridCell.setup(renderCell: boardViewModel
            .cellForItem(at: indexPath.oneDimensionalIndexFromSectionRow()))
        
        return cell
    }
    
    
}

extension BoardViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let spacing = CGFloat(integerLiteral: 6)
        let widthMinusSpacing = collectionView.bounds.width - spacing - spacing
        return CGSize(width: widthMinusSpacing / 3, height: widthMinusSpacing / 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(integerLiteral: 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(integerLiteral: 3)
    }
}

extension IndexPath {
    func oneDimensionalIndexFromSectionRow() -> Int {
        return (section * 3) + row
    }
}

