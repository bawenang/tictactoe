//
//  BoardViewModel.swift
//  tictactoe
//
//  Created by Bawenang RPP on 18/06/19.
//  Copyright © 2019 Bawenang. All rights reserved.
//

import RxSwift
import RxCocoa

class BoardViewModel {
    
    private var currentPlayer = ActivePlayer.playerOne
    
    private var boardLogic: BoardLogic
    private var isPlaying = true
    
    let eventActivePlayer = PublishRelay<ActivePlayer?>()
    let eventSetBoard = PublishSubject<Void>()
    let eventPlayerOneWins = PublishSubject<Void>()
    let eventPlayerTwoWins = PublishSubject<Void>()
    let eventDraw = PublishSubject<Void>()
    
    init(boardLogic: BoardLogic) {
        self.boardLogic = boardLogic
    }
    
    func viewDidLoad() {
        currentPlayer = .playerOne
        eventSetBoard.onNext(())
        eventActivePlayer.accept(.playerOne)
    }
    
    func cellForItem(at index: Int) -> RenderCell {
        return boardLogic.getRenderCell(at: index)
    }
    
    func didSelectCell(at index: Int) {
        if isPlaying {
            let isCurrentlyPlayerOne = (currentPlayer == .playerOne)
            
            do {
                try boardLogic.chooseCell(index: index, for: currentPlayer)
                eventSetBoard.onNext(())
                
                let winningState = boardLogic.getWinningState()
                if winningState == .none {
                    currentPlayer = isCurrentlyPlayerOne ? .playerTwo : .playerOne
                    eventActivePlayer.accept(currentPlayer)
                } else if winningState == .playerOne {
                    isPlaying = false
                    eventPlayerOneWins.onNext(())
                } else if winningState == .playerTwo {
                    isPlaying = false
                    eventPlayerTwoWins.onNext(())
                } else if winningState == .draw {
                    isPlaying = false
                    eventDraw.onNext(())
                }
            } catch {
                
            }
        }
    }
}
