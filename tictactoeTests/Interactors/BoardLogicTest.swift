//
//  BoardLogicTest.swift
//  tictactoeTests
//
//  Created by Bawenang RPP on 21/06/19.
//  Copyright © 2019 Bawenang. All rights reserved.
//

import XCTest

@testable import tictactoe

class BoardLogicTest: XCTestCase {

    func testChooseCell() {
        let boardMock = BoardMock()
        let boardLogic = BoardLogicImpl(board: boardMock)
        
        doAndCatchFailingError {
            try boardLogic.chooseCell(index: 0, for: .playerOne)
            XCTAssertTrue(boardMock.setCellCalled)
        }
        
        doAndCatchFailingError {
            try boardLogic.chooseCell(index: 1, for: .playerTwo)
            XCTAssertTrue(boardMock.setCellCalled)
        }
    }

    func testChooseCellWhenThrowError() {
        let boardMock = BoardMock()
        boardMock.shouldThrowError(true)
        let boardLogic = BoardLogicImpl(board: boardMock)
        
        do {
            let _ = try boardLogic.chooseCell(index: 0, for: .playerOne)
            
            XCTFail("Should not get here and catch LogicError.chooseNotEmptyCell instead")
        } catch let error {
            guard let error = error as? LogicError else {
                XCTFail("Wrong typ of error caught")
                return
            }
            XCTAssertEqual(error, LogicError.chooseNotEmptyCell)
        }
    }

    func testGetRenderCell() {
        let gridMock: [Cell] = [
            .playerOne, .playerTwo, .empty,
            .empty, .playerOne, .playerTwo,
            .playerTwo, .empty, .playerOne
        ]
        let boardMock = BoardMock()
        boardMock.setGrid(grid: gridMock)
        let boardLogic = BoardLogicImpl(board: boardMock)
        
        XCTAssertEqual(boardLogic.getRenderCell(at: 0), .x)
        XCTAssertEqual(boardLogic.getRenderCell(at: 1), .o)
        XCTAssertEqual(boardLogic.getRenderCell(at: 2), .empty)
        
    }

    func testGetWinningStateNone() {
        let gridMock: [Cell] = [
            .playerOne, .playerTwo, .empty,
            .empty, .empty, .empty,
            .empty, .empty, .empty
        ]
        let boardMock = BoardMock()
        boardMock.setGrid(grid: gridMock)
        let boardLogic = BoardLogicImpl(board: boardMock)
        
        XCTAssertEqual(boardLogic.getWinningState(), .none)
        
    }

    func testGetWinningStateDrawWhenWinningStateNoneAndNoEmptySpace() {
        let drawGrid: [Cell] = [
            .playerOne, .playerOne, .playerTwo,
            .playerTwo, .playerTwo, .playerOne,
            .playerOne, .playerOne, .playerTwo
        ]
        let boardMock = BoardMock()
        boardMock.setGrid(grid: drawGrid)
        let boardLogic = BoardLogicImpl(board: boardMock)
        
        XCTAssertEqual(boardLogic.getWinningState(), .draw)
    }
    
    func testGetWinningStatePlayerOne() {
        assertWinningState(for: [0, 1, 2], cellType: .playerOne, winningPlayer: .playerOne)
        assertWinningState(for: [3, 4, 5], cellType: .playerOne, winningPlayer: .playerOne)
        assertWinningState(for: [6, 7, 8], cellType: .playerOne, winningPlayer: .playerOne)
        assertWinningState(for: [0, 3, 6], cellType: .playerOne, winningPlayer: .playerOne)
        assertWinningState(for: [1, 4, 7], cellType: .playerOne, winningPlayer: .playerOne)
        assertWinningState(for: [2, 5, 8], cellType: .playerOne, winningPlayer: .playerOne)
        assertWinningState(for: [0, 4, 8], cellType: .playerOne, winningPlayer: .playerOne)
        assertWinningState(for: [2, 4, 6], cellType: .playerOne, winningPlayer: .playerOne)
    }
    
    func testGetWinningStatePlayerTwo() {
        assertWinningState(for: [0, 1, 2], cellType: .playerTwo, winningPlayer: .playerTwo)
        assertWinningState(for: [3, 4, 5], cellType: .playerTwo, winningPlayer: .playerTwo)
        assertWinningState(for: [6, 7, 8], cellType: .playerTwo, winningPlayer: .playerTwo)
        assertWinningState(for: [0, 3, 6], cellType: .playerTwo, winningPlayer: .playerTwo)
        assertWinningState(for: [1, 4, 7], cellType: .playerTwo, winningPlayer: .playerTwo)
        assertWinningState(for: [2, 5, 8], cellType: .playerTwo, winningPlayer: .playerTwo)
        assertWinningState(for: [0, 4, 8], cellType: .playerTwo, winningPlayer: .playerTwo)
        assertWinningState(for: [2, 4, 6], cellType: .playerTwo, winningPlayer: .playerTwo)
    }
    
    private func doAndCatchFailingError(block: () throws -> (Void)) {
        do {
            try block()
        } catch {
            XCTFail("Test failed")
        }
    }

    private func assertWinningState(for cellIndices: [Int], cellType: Cell, winningPlayer: WinningState) {
        let sampleGrid = buildSampleGrid(cellIndices: cellIndices, of: cellType)
        let boardMock = BoardMock()
        boardMock.setGrid(grid: sampleGrid)
        let boardLogic = BoardLogicImpl(board: boardMock)
        
        XCTAssertEqual(boardLogic.getWinningState(), winningPlayer)
    }

    private func buildSampleGrid(cellIndices: [Int], of cellType: Cell) -> [Cell] {
        var grid = Array<Cell>(repeating: .empty, count: 9)
        
        grid = grid.enumerated().map { index, cell -> Cell in
            if cellIndices.contains(index) {
                return cellType
            } else {
                return .empty
            }
        }
        
        return grid
    }
    
    private class BoardMock: Board {
        private(set) var getCellCalled = false
        private(set) var setCellCalled = false
        private var throwError = false
        private var grid: [Cell]?
        
        func getCell(at index: Int) -> Cell {
            return grid?[index] ?? .empty
        }
        
        func getGrid() -> [Cell] {
            return grid ?? Array<Cell>(repeating: .empty, count: 9)
        }
        
        func setCell(at index: Int, cellType: Cell) throws {
            if throwError {
                throw LogicError.chooseNotEmptyCell
            }
            setCellCalled = true
        }
        
        func hasEmptyCell() -> Bool {
            return false
        }
        
        func shouldThrowError(_ shouldError: Bool) {
            throwError = shouldError
        }
        
        func setGrid(grid: [Cell]) {
            self.grid = grid
        }
    }
}
