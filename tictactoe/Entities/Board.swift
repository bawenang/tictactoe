//
//  Board.swift
//  tictactoe
//
//  Created by Bawenang RPP on 21/06/19.
//  Copyright © 2019 Bawenang. All rights reserved.
//

enum Cell {
    case empty
    case playerOne
    case playerTwo
}

protocol Board {
    func getCell(at index: Int) -> Cell
    func getGrid() -> [Cell] 
    func setCell(at index: Int, cellType: Cell) throws
}

class BoardImpl: Board {
    private var grid: [Cell]
    
    init?(grid: [Cell]) {
        if grid.count != 9 {
            return nil
        }
        
        self.grid = grid
    }
    
    func getCell(at index: Int) -> Cell {
        return grid[index]
    }
    
    func getGrid() -> [Cell] {
        return grid
    }
    
    func setCell(at index: Int, cellType: Cell) throws {
        if grid[index] != .empty {
            throw LogicError.chooseNotEmptyCell
        }
        
        grid[index] = cellType
    }
}

extension Cell {
    static func create(from activePlayer: ActivePlayer) -> Cell {
        if activePlayer == .playerOne {
            return .playerOne
        } else if activePlayer == .playerTwo {
            return .playerTwo
        }
        
        return .empty
    }
}

extension BoardImpl {
    static func createDefault() -> Board {
        let grid = Array<Cell>(repeating: .empty, count: 9)
        return BoardImpl(grid: grid)!
    }
}
